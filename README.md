# GTFS-RT Viewer
Demo Vue application built to fetch and decode an GTFS-RT feed. Currently a work in progress.
Uses a cors escape proxy to get around cors protocols, therefore NOT suitable for production applications.
Just meant to test a feed and convert it to something readable.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

## Known bugs

- Search function will loop through prototype and observer objects, and possibly overflow.
- Browser or tab will crash or become unresponsive if the object it's trying to display is too big.